const Expense = require("../database/schemas/expense");
require("@test/auth/database/schemas/user");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;

const LIMIT = 10;

async function getTotalExpenses(userId, startDate, endDate) {
  const match = {};

  if (startDate) {
    match = { $gte: new Date(startDate), $lt: end };
  }
  let end = new Date();
  if (endDate) {
    end = new Date(endDate);
  }

  let start = startDate ? new Date(startDate) : null;
  resultAgre = await Expense.aggregate([
    {
      $match: {
        $and: [
          { authorId: ObjectId(userId) },
          { date: { $gte: start, $lt: end } },
        ],
      },
    },
    {
      $group: {
        _id: null,
        value: { $sum: "$value" },
        count: { $sum: 1 },
      },
    },
  ]);

  return resultAgre[0];
}

async function getTotalExpenseWithFilters(req, res) {
  const { id } = req.params;
  console.log();
  const result = await getTotalExpenses(
    id,
    req.headers["startdate"],
    req.headers["enddate"]
  );
  console.log(result)
  res.json(result).end();
}

async function create(req, res) {
  const { title, description, authorId, value, date } = req.body;

  const expense = new Expense({ title, description, authorId, value, date });
  expense
    .save()
    .then((expense) => {
      res.send(expense);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
}

function remove(req, res) {
  const { id } = req.body;
  Expense.findByIdAndDelete(id)
    .then(() => res.json({ message: `Expense ${id} removed` }))
    .catch((err) => res.status(500).send(err));
}

function findAll(req, res) {
  Expense.find()
    .populate("User")
    .then((expenses) => {
      res.send(expenses);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
}

function findById(req, res) {
  const { id } = req.params;
  Expense.findById(id)
    .populate("authorId")
    .exec()
    .then((expense) => {
      res.send(expense);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
}

async function findByAuthorId(req, res) {
  const { id } = req.params;

  const page = parseInt(req.query.page);
  const skipIndex = (page - 1) * LIMIT;
  const results = {};
  try {
    results.total = await Expense.count({ authorId: id }).exec();
    results.totalPages = Math.round(results.total / LIMIT);
    results.results = await Expense.find({ authorId: id })
      .populate("authorId")
      .sort({ _id: 1 })
      .limit(LIMIT)
      .skip(skipIndex)
      .exec();
    console.log(results);
    res.json(results).end();
  } catch (e) {
    res.status(500).json({ message: "Error Occured" }).end();
  }
}

module.exports = {
  create,
  remove,
  findAll,
  findById,
  findByAuthorId,
  getTotalExpenseWithFilters,
};
