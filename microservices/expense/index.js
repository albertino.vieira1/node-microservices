require('dotenv').config();
require("./database/index").connect()
const express = require('express')
const app = express()
const routes = require("./routes/routes")

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use("/expense",routes)

app.listen(process.env.PORT | 3003, () => {
    console.log(`Expense app listening at http://localhost:${process.env.PORT}`)
  })
