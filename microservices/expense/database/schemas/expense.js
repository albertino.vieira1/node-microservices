const mongoose = require("mongoose");

const expenseSchema = new mongoose.Schema({
  description: String,
  title: String,
  authorId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  value: Number,
  date: { type: Date, required: true }
});

module.exports = mongoose.model("Expense", expenseSchema);
