const Router = require("express");
const expenseController = require("../controllers/expenseController")
const router = Router();

router.post("/",expenseController.create);
router.delete("/",expenseController.remove);
router.get("/all",expenseController.findAll);
router.get("/:id",expenseController.findById);
router.get("/author/:id",expenseController.findByAuthorId);
router.get("/total/author/:id",expenseController.getTotalExpenseWithFilters);

router.get("/ping", (req, res) => res.send({message: 'pong!'}))
router.use('/*', (req, res) => res.status(404).json({ message: "Not Found" }).end())


module.exports = router;