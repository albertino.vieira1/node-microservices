require('dotenv').config();
require("./database/index").connect()
const express = require('express')
const app = express()
const routes = require("./routes/routes")

app.use(express.json())
app.use("/task",routes)

app.listen(process.env.PORT | 3002, () => {
    console.log(`Task app listening at http://localhost:${process.env.PORT}`)
  })
