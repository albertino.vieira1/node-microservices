const Task = require("../database/schemas/task");
require("@test/auth/database/schemas/user")

async function create(req, res) {
  const { title, description, authorId } = req.body;

  const task = new Task({ title, description, authorId });
  task
    .save()
    .then((task) => {
      res.send(task);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
}

function remove(req, res) {
  const { id } = req.body;
  Task.findByIdAndDelete(id)
    .then(() => res.json({message: `Task ${id} removed`}))
    .catch((err) => res.status(500).send(err));
}

function findAll(req, res) {
    Task.find().populate("User")
    .then((tasks) => {
      res.send(tasks);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
}

function findById(req, res) {
    const { id } = req.params;
    Task.findById(id).populate("authorId").exec().then((tasks) => {
      res.send(tasks);
    })
    .catch((err) => {
        console.log(err)
      res.status(500).send(err);
    });
}

module.exports = { create, remove, findAll, findById };
