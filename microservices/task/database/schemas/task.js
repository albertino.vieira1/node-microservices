const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
  description: String,
  title: String,
  authorId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
});

module.exports = mongoose.model("Task", taskSchema);
