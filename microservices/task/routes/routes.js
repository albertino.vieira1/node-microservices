const Router = require("express");
const taskController = require("../controllers/taskController")
const router = Router();

router.post("/",taskController.create);
router.delete("/",taskController.remove);
router.get("/all",taskController.findAll);
router.get("/:id",taskController.findById);
router.get("/ping", (req, res) => res.send({message: 'pong!'}))
router.use('/*', (req, res) => res.status(404).json({ message: "Not Found" }).end())


module.exports = router