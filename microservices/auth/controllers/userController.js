const User = require("../database/schemas/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

async function create(req, res) {
  const { email, password, name } = req.body;

  const userExists = await User.findOne({
    name: name,
    email: email,
    password: password,
  });
  if (userExists) {
    res.status(500).send("User Already Exists!");
  } else {
    //Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);
    const user = new User({
      name: name,
      email: email,
      password: encryptedPassword,
    });
    user
      .save()
      .then((user) => {
        const token = jwt.sign(
          { user_id: user._id, email },
          process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }
        );
        // save user token
        user.token = token;
        res.send(user);
      })
      .catch((err) => {
        console.log(err)
        res.status(500).send("erro");
      });
  }
}

function remove(req, res) {
  const { email } = req.body;
  User.findOneAndDelete({ email: email })
    .then(() => res.send(user))
    .catch((err) => res.status(500).send(err));
}

function findAll(req, res) {
  User.find()
    .then((users) => {
      res.send(users);
    })
    .catch((err) => {
      res.status(500).send(err);
    });
}

async function login(req, res) {
  console.log("enter")
  try {
    // Get user input
    const { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }
    // Validate if user exist in our database
    const user = await User.findOne({ email }).select("+password");

    if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      // save user token
      user.token = token;
      user.password = undefined;
      // user
      res.status(200).json(user);
    } else {
      res.status(400).send("Invalid Credentials");
    }
  } catch (err) {
    console.log(err);
  }
}

module.exports = { create, remove, findAll, login };
