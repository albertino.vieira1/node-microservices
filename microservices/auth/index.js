require('dotenv').config();
require("./database/index").connect()
const express = require('express')
const app = express()
const routes = require("./routes/routes")

app.use(express.json())
app.use("/",routes)

app.listen(process.env.PORT | 3001, () => {
    console.log(`Example app listening at http://localhost:${process.env.PORT}`)
  })
