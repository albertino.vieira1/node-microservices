const Router = require("express");
const userController = require("../controllers/userController")
const router = Router();

router.post("/auth/user",userController.create);
router.delete("/auth/user",userController.remove);
router.get("/auth/users",userController.findAll);
router.post("/auth/login",userController.login);
router.get("/auth/ping", (req, res) => res.send({message: 'pong!'}))
router.use('/auth/*', (req, res) => res.status(404).json({ message: "Not Found" }).end())


module.exports = router