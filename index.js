require('dotenv').config();
const httpProxy = require("http-proxy");
const services = require("./config-services.json");
const express = require("express");
const app = express();
const cors = require('cors')
app.use(cors())
const getMicroservice = (key) => {
  let microserviceKey = key.split("/");
  const microservice = services.find(
    (element) => element.key === microserviceKey[1]
  );
  return microservice ? microservice.port : null;
};

const server = httpProxy.createProxyServer();
/**
 * All routes enter here 
 */
app.all("/*", (req, res) => {
  const port = getMicroservice(req.url);
  if (port) {
    const target = `${process.env.BASE_URL}${port}`;
    server.web(req, res, { target: target }, (err) => console.log(err));
  } else {
    res.status(404).json({ message: "Not Found" }).end();
  }
});

app.listen(process.env.PORT | 5050, () => {
  console.log(`Example app listening at http://localhost:${process.env.PORT}`);
});
